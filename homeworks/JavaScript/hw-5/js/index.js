function createNewUser() {

    const newUser = {}
    const newFirstName = prompt("Введите имя пользователя : ");
    const newLastName = prompt("Введите фамилию пользователя : ");
    const newBirthday = prompt("Введите дату рождения (текст в формате dd.mm.yyyy) : ");

    newUser.firstName = newFirstName;
    newUser.lastName = newLastName;
    newUser.birthday = newBirthday;
    newUser.getLogin = function()
    {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    };
    newUser.getAge = function() {
        if (new Date().getMonth() + 1 - +this.birthday.slice(3, 5) > 0 || (new Date().getMonth() + 1 - +this.birthday.slice(3, 5) === 0 && (new Date().getDate() - +this.birthday.slice(0, 2) >= 0))) {
            return [new Date().getFullYear()] - +this.birthday.slice(-4)
        } else { return [new Date().getFullYear()] - +this.birthday.slice(-4) - 1 }
    };
     newUser.getPassword = function() {
         return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + +this.birthday.slice(-4)
     }
    return newUser;
}

const user = createNewUser();
console.log(user);

console.log(`Предлагаемый логин : ${user.getLogin()}, 
возраст пользователя : ${user.getAge()} , 
предлагаемый пароль : ${user.getPassword()}`);