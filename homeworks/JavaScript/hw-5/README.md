## Ответ на теоретический вопрос к HomeWork - 5 по JavaScript

### Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
    

-   используя экранирование, мы избегаем специальные символы, когда не хотим, чтобы они имели свое особое значение.

-  Экранирование символов означает, что мы делаем что-то с ними, чтобы убедиться, что они распознаются как текст, а не часть кода. В JavaScript это достигается путем добавления обратной косой черты непосредственно перед символом.

  