const firstNum = +prompt( 'Введите первое число: ');
const secondNum = +prompt( 'Введите второе число: ');
const operation = prompt( 'Введите математическую операцию, которую нужно совершить. Сюда может быть введено + , - , * или / : ');
const calc = function (num1 ,num2, oper){
    switch (oper) {
        case "+" : {return num1 + num2}
        case "-" : {return num1 - num2}
        case "*" : {return num1 * num2}
        case "/" : {return num1 / num2}
    }
}

console.log(`Результат: ${firstNum} ${operation} ${secondNum} = ${calc(firstNum, secondNum, operation)}`)
