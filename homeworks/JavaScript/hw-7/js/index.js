const elems1 = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const elems2 = ['1', '2', '3', 'sea', 'user', 23];

const body = document.querySelector(".page-body");

function listItems(items) {
    let ul = document.createElement('ul');
    const newItemArr = items.map((element) => {
        let li = document.createElement('li');
        li.textContent = element;
        return li;
    });
    newItemArr.forEach((el) => {
        ul.append(el);
    });
    body.append(ul);
}
listItems(elems1);

listItems(elems2);
