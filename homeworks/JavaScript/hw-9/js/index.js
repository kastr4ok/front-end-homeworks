// Технические требования:
//
//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

const ul = document.getElementsByClassName("tabs")[0];
const ulText = document.getElementsByClassName("tabs-content")[0];


// function tabsArray(items) {
//     const newTabsArr = items.map((element) => {
//         return `<li> ${element} </li>`;
//     });
//     let list = "<ul>";
//     newTabsArr.forEach((el) => {
//         list += el;
//     });
//     body.innerHTML += list;
// }

console.log(ul.children);
console.log(ulText);


// const Akali = document.createElement("li");
//     Akali.textContent = "Abandoning the Kinkou Order and her title of the Fist of Shadow, Akali now strikes alone, ready to be the deadly weapon her people need. Though she holds onto all she learned from her master Shen, she has pledged to defend Ionia from its enemies, one kill at a time. Akali may strike in silence, but her message will be heard loud and clear: fear the assassin with no maste";
// const Anivia = document.createElement("li");
//     Anivia.textContent = "Anivia is a benevolent winged spirit who endures endless cycles of life, death, and rebirth to protect the Freljord. A demigod born of unforgiving ice and bitter winds, she wields those elemental powers to thwart any who dare disturb her homeland. Anivia guides and protects the tribes of the harsh north, who revere her as a symbol of hope, and a portent of great change. She fights with every ounce of her being, knowing that through her sacrifice, her memory will endure, and she will be reborn into a new tomorrow.";
// const Draven = document.createElement("li");
//     Draven.textContent = "In Noxus, warriors known as reckoners face one another in arenas where blood is spilled and strength tested—but none has ever been as celebrated as Draven. A former soldier, he found that the crowds uniquely appreciated his flair for the dramatic, not to mention the spray of blood from each of his spinning axes. Addicted to the spectacle of his own brash perfection, Draven has sworn to defeat whomever he must to ensure that his name is chanted throughout the empire forever more.";
// const Garen = document.createElement("li");
//     Garen.textContent = "A proud and noble soldier, Garen fights at the head of the Dauntless Vanguard. He is popular among his fellows, and respected well enough by his enemies—not least as a scion of the prestigious Crownguard family, entrusted with defending Demacia and its ideals. Clad in magic-resistant armor and bearing a mighty broadsword, Garen stands ready to confront mages and sorcerers on the field of battle, in a veritable whirlwind of righteous steel.";
// const Katarina = document.createElement("li");
//     Katarina.textContent = "Decisive in judgment and lethal in combat, Katarina is a Noxian assassin of the highest caliber. Eldest daughter to the legendary General Du Couteau, she made her talents known with swift kills against unsuspecting enemies. Her fiery ambition has driven her to pursue heavily-guarded targets, even at the risk of endangering her allies—but no matter the mission, Katarina will not hesitate to execute her duty amid a whirlwind of serrated daggers.";
//


ul.addEventListener("click", (event) => {
const text = "";
    // document.body.ulText.remove();
    const li = event.target.closest("li");
    const children = ul.children;

    li.classList.add("active");
    const itm = li.textContent;
    console.log(itm);
    for (let item of children) {
        if (item !== li) {
            item.classList.remove("active");
        }
    }
    ul.append(text);
});
