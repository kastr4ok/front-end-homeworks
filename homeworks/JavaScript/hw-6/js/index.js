const arr = ['hello', 'world', 23, '23', null];
const arr2 = ['hello', 45, false, 'world', 23, undefined, '23', null, 'undefined', true, 0, NaN, ''];//другой (дополнительный) вариант
const ExcludeType = 'string';
const ExcludeType2 = 'boolean';//другой (дополнительный) вариант
const ExcludeType3 = 'number';//другой (дополнительный) вариант

const filterBy = function (items, findType) {
   return  items.filter((element) => typeof(element) !== findType );
}

console.log(filterBy(arr, ExcludeType));
console.log(filterBy(arr2, ExcludeType2));//другой (дополнительный) вариант
console.log(filterBy(arr2, ExcludeType3));//другой (дополнительный) вариант