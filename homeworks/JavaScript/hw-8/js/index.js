const body = document.querySelector(".page-body");

document.addEventListener("DOMContentLoaded", function(event) {
    const input = document.createElement('input');
    input.type = "number";
    input.style.margin = "10px";
    input.placeholder = "Price";

    document.body.append(input);
    input.addEventListener("focus", (evt) => {
        evt.target.style.cssText = "border: 3px solid #0f0";
        evt.target.style.outline = "transparent";
        evt.target.style.name = "number";
        evt.target.style.margin = "10px";
        clearValue(input);
        div.remove();
        spn.remove();
    });

    function valueOf(name) {
        return name.value;
    }

    function clearValue(name) {
        return name.value ="";
    }

    const span = document.createElement("span");

    const btn = document.createElement("button");
    btn.innerText = "X";
    btn.style.cssText = "border: 3px solid #000";
    btn.style.marginLeft = "10px";
    btn.style.padding = "2px 5px";
    btn.style.cursor = "pointer";

    const div = document.createElement("div");
    div.style.margin = "10px";
    div.prepend(span);
    div.append(btn);

    const spn = document.createElement("span");

    input.addEventListener("blur", (evt) => {
        const num = valueOf(input);
        console.log(num);
        if (num < 0 || num === ""){
            evt.target.style.cssText = "border: 3px solid #f00";
            spn.textContent = "Please enter correct price.";
            spn.style.display = "block";
            input.after(spn);
        }
        else {
            evt.target.style.cssText = "border: transparent";
            evt.target.style.cssText = "color: #0f0";
            span.textContent = (`Текущая цена: ${num}`);
            document.body.insertBefore(div, input);
        }
    });

    btn.addEventListener("click", (evt) => {
        const target = evt.target;
        if (target.tagName === "BUTTON") {
            div.remove();
            clearValue(input);
        }
    })
    ;
});

